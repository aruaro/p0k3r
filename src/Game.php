<?php

namespace Poker;

class Game
{
    const NUM_PUBLIC_CARDS = 2;
    const NUM_PRIVATE_CARDS = 3;

    private $deck;

    private function __construct()
    {
        $this->deck = Deck::create();
    }

    public static function start()
    {

    }

    public function end()
    {

    }
}
