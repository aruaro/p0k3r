<?php

namespace Poker;

class Deck
{
    private static $cards;

    private function __construct()
    {
        if (static::$cards) {
            return;
        }

        foreach (Card::SUITS as $suit => $_) {
            foreach (Card::RANKS as $rank => $_) {
                static::$cards[] = new Card($suit, $rank);
            }
        }
    }

    public static function create()
    {
        static::$cards = [];

        return new static();
    }

    public function getCardCount()
    {
        return count(static::$cards);
    }

    public function shuffle()
    {
        if (!static::$cards) {
            throw new \LogicException('No cards in deck');
        }

        shuffle(static::$cards);
    }

    public function draw(int $count = 1)
    {
        if (!static::$cards) {
            throw new \LogicException('No cards in deck');
        }

        if ($count == 0) {
            throw new \LogicException('Cannot draw nothing');
        }

        if ($count > $this->getCardCount()) {
            throw new \LogicException(
                'Cannot draw more than ' . $this->getCardCount() . ' cards'
            );
        }

        $cards = [];

        for ($x = 1; $x <= $count; $x++) {
            $cards[] = array_shift(static::$cards);
        }

        return $cards;
    }
}
