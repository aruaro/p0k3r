<?php

namespace Poker;

class Player
{
    const ROYAL_FLUSH    = 'royal flush';
    const STRAIGHT_FLUSH = 'straight flush';
    const FOUR_OF_KIND   = 'four of a kind';
    const FULL_HOUSE     = 'full house';
    const FLUSH          = 'flush';
    const STRAIGHT       = 'straight';
    const THREE_OF_KIND  = 'three of a kind';
    const TWO_PAIR       = 'two pair';
    const ONE_PAIR       = 'one pair';
    const HIGH_CARD      = 'high card';

    const SCORES = [
        self::ROYAL_FLUSH    => 10000,
        self::STRAIGHT_FLUSH => 9000,
        self::FOUR_OF_KIND   => 8000,
        self::FULL_HOUSE     => 7000,
        self::FLUSH          => 6000,
        self::STRAIGHT       => 5000,
        self::THREE_OF_KIND  => 4000,
        self::TWO_PAIR       => 3000,
        self::ONE_PAIR       => 2000,
        self::HIGH_CARD      => 1000,
    ];

    private $hand  = self::HIGH_CARD;
    private $cards = [];

    public function __construct(array $cards = [])
    {
        if (count($cards) != 5) {
            throw new \RuntimeException('Hand must have 5 cards');
        }

        foreach ($cards as $card) {
            $this->addCard($card);
        }

        ksort($this->cards);

        $this->permutate();
    }

    public function getHand()
    {
        return $this->hand;
    }

    public function getHighCard()
    {
        return end($this->cards);
    }

    public function getScore()
    {
        return self::SCORES[$this->hand] + $this->getHighCard()->getScore();
    }

    private function addCard(Card $card)
    {
        $this->cards[$card->getScore()] = $card;
    }

    private function permutate()
    {
        $permutations = [
            'isRoyalFlush',
            'isStraightFlush',
            'isFourOfKind',
            'isFullHouse',
            'isFlush',
            'isStraight',
            'isThreeOfKind',
            'isTwoPair',
            'isOnePair',
        ];

        foreach ($permutations as $permutation) {
            if ($this->$permutation()) {
                return;
            }
        }
    }

    private function isRoyalFlush()
    {
        if (!$this->isFlush()) {
            return false;
        }

        $ranks = ['ace', 'king', 'queen', 'jack', '10'];

        foreach ($this->cards as $card) {
            if (!in_array($card->getRank(), $ranks)) {
                return false;
            }
        }

        $this->hand = self::ROYAL_FLUSH;

        return true;
    }

    private function isStraightFlush()
    {
        if (!$this->isFlush()) {
            return false;
        }

        if (!$this->isStraight()) {
            return false;
        }

        $this->hand = self::STRAIGHT_FLUSH;

        return true;
    }

    private function isFourOfKind()
    {
        $group = $this->sortCards();

        foreach ($group as $cards) {
            if (count($cards) == 4) {
                $this->hand = self::FOUR_OF_KIND;

                return true;
            }
        }

        return false;
    }

    private function isFullHouse()
    {
        $group = $this->sortCards();

        if (count($group) != 2) {
            return false;
        }

        foreach ($group as $cards) {
            if (!(count($cards) == 2 || count($cards) == 3)) {
                return false;
            }
        }

        $this->hand = self::FULL_HOUSE;

        return true;
    }

    private function isFlush()
    {
        $suit = '';

        foreach ($this->cards as $card) {
            if (!$suit) {
                $suit = $card->getSuit();
            } elseif ($suit != $card->getSuit()) {
                return false;
            }
        }

        $this->hand = self::FLUSH;

        return true;
    }

    private function isStraight()
    {
        $cards = $this->sortCards();

        $current = false;

        foreach ($cards as $rank => $_) {
            if ($current !== false && $current + 1 != $rank) {
                return false;
            }

            $current = $rank;
        }

        $this->hand = self::STRAIGHT;

        return true;
    }

    private function isThreeOfKind()
    {
        $group = $this->sortCards();

        foreach ($group as $cards) {
            if (count($cards) == 3) {
                $this->hand = self::THREE_OF_KIND;

                return true;
            }
        }

        return false;
    }

    private function isTwoPair()
    {
        $group = $this->sortCards();

        if (count($group) != 3) {
            return false;
        }

        foreach ($group as $cards) {
            if (!(count($cards) == 2 || count($cards) == 1)) {
                return false;
            }
        }

        $this->hand = self::TWO_PAIR;

        return true;
    }

    private function isOnePair()
    {
        $group = $this->sortCards();

        foreach ($group as $cards) {
            if (count($cards) == 2) {
                $this->hand = self::ONE_PAIR;

                return true;
            }
        }

        return false;
    }

    private function sortCards($sort = 'rank')
    {
        $ranks = array_flip(array_keys(Card::RANKS));
        $cards = [];

        foreach ($this->cards as $card) {
            $cards[$ranks[$card->getRank()]][$card->getSuit()] = $card;
        }

        ksort($cards);

        return $cards;
    }
}
