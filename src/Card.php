<?php

namespace Poker;

class Card
{
    const SUITS = [
        'club'    => 100,
        'spade'   => 200,
        'diamond' => 300,
        'heart'   => 400
    ];

    const RANKS = [
        'ace'   => 12,
        '2'     => 13,
        '3'     => 1,
        '4'     => 2,
        '5'     => 3,
        '6'     => 4,
        '7'     => 5,
        '8'     => 6,
        '9'     => 7,
        '10'    => 8,
        'jack'  => 9,
        'queen' => 10,
        'king'  => 11,
    ];

    private $suit;
    private $rank;

    public function __construct(string $suit, string $rank)
    {
        if (!isset(self::SUITS[$suit])) {
            throw new \InvalidArgumentException('Invalid suit');
        }

        if (!isset(self::RANKS[$rank])) {
            throw new \InvalidArgumentException('Invalid rank');
        }

        $this->suit = $suit;
        $this->rank = $rank;
    }

    public function getSuit() : string
    {
        return $this->suit;
    }

    public function getRank() : string
    {
        return $this->rank;
    }

    public function getScore() : int
    {
        return self::SUITS[$this->suit] + self::RANKS[$this->rank];
    }
}
