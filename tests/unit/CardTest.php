<?php

namespace Poker\Test\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Poker\Card;

class CardTest extends TestCase
{
    public function createCardProvider()
    {
        return [
            'ace of hearts' => ['heart', 'ace', true],
            'invalid card'  => ['nope', 'nope', false]
        ];
    }

    /**
     * @dataProvider createCardProvider
     */
    public function testHasValidSuitAndRank(string $suit, string $rank, bool $valid)
    {
        if (!$valid) {
            $this->expectException(\InvalidArgumentException::class);
        }

        $card = new Card($suit, $rank);

        if ($valid) {
            $this->assertEquals('heart', $card->getSuit());
            $this->assertEquals('ace', $card->getRank());
        }
    }

    public function testHasScoreGivenSuitAndRank()
    {
        $card = new Card('heart', 'ace');

        $this->assertEquals(412, $card->getScore());
    }
}
