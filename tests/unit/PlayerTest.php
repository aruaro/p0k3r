<?php

namespace Poker\Test\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Poker\Card;
use Poker\Deck;
use Poker\Player;

class PlayerTest extends TestCase
{
    public function handProvider()
    {
        return [
            '3 cards' => [Deck::create()->draw(3)],
            '5 cards' => [Deck::create()->draw(5)],
            '7 cards' => [Deck::create()->draw(7)]
        ];
    }

    /**
     * @dataProvider handProvider
     */
    public function testShouldHave5Cards(array $cards)
    {
        if (count($cards) != 5) {
            $this->expectException(\RuntimeException::class);
        } else {
            $this->expectNotToPerformAssertions();
        }

        $player = new Player($cards);
    }


    public function bestHandProvider()
    {
        return [
            Player::ROYAL_FLUSH => [
                'hand'  => Player::ROYAL_FLUSH,
                'cards' => [
                    ['heart', 'ace'],
                    ['heart', 'king'],
                    ['heart', 'queen'],
                    ['heart', 'jack'],
                    ['heart', '10'],
                ]
            ],
            Player::STRAIGHT_FLUSH => [
                'hand'  => Player::STRAIGHT_FLUSH,
                'cards' => [
                    ['heart', '2'],
                    ['heart', '3'],
                    ['heart', '4'],
                    ['heart', '5'],
                    ['heart', '6'],
                ]
            ],
            Player::FOUR_OF_KIND => [
                'hand'  => Player::FOUR_OF_KIND,
                'cards' => [
                    ['heart', 'ace'],
                    ['diamond', 'ace'],
                    ['club', 'ace'],
                    ['spade', 'ace'],
                    ['heart', '10'],
                ]
            ],
            Player::FULL_HOUSE => [
                'hand'  =>  Player::FULL_HOUSE,
                'cards' => [
                    ['heart', 'ace'],
                    ['diamond', 'ace'],
                    ['club', 'ace'],
                    ['heart', '10'],
                    ['club', '10'],
                ]
            ],
            Player::FLUSH => [
                'hand'  => Player::FLUSH,
                'cards' => [
                    ['heart', '3'],
                    ['heart', '5'],
                    ['heart', '7'],
                    ['heart', '9'],
                    ['heart', '10'],
                ]
            ],
            Player::STRAIGHT => [
                'hand'  => Player::STRAIGHT,
                'cards' => [
                    ['heart', '3'],
                    ['diamond', '4'],
                    ['club', '5'],
                    ['spade', '6'],
                    ['heart', '7'],
                ]
            ],
            Player::THREE_OF_KIND => [
                'hand'  => Player::THREE_OF_KIND,
                'cards' => [
                    ['heart', 'ace'],
                    ['diamond', 'ace'],
                    ['club', 'ace'],
                    ['heart', '10'],
                    ['club', '9'],
                ]
            ],
            Player::TWO_PAIR => [
                'hand'  => Player::TWO_PAIR,
                'cards' => [
                    ['heart', 'ace'],
                    ['diamond', 'ace'],
                    ['club', '10'],
                    ['heart', '10'],
                    ['club', '8'],
                ]
            ],
            Player::ONE_PAIR => [
                'hand'  => Player::ONE_PAIR,
                'cards' => [
                    ['heart', 'ace'],
                    ['diamond', 'ace'],
                    ['club', '10'],
                    ['heart', '9'],
                    ['club', '8'],
                ]
            ],
            Player::HIGH_CARD => [
                'hand'  => Player::HIGH_CARD,
                'cards' => [
                    ['heart', '3'],
                    ['diamond', '5'],
                    ['club', '7'],
                    ['heart', '9'],
                    ['club', '10'],
                ]
            ],
        ];
    }

    /**
     * @dataProvider bestHandProvider
     */
    public function testShouldFindBestHand(string $hand, array $cards)
    {
        $cards = array_map(function ($card) {
            return new Card($card[0], $card[1]);
        }, $cards);

        $player = new Player($cards);

        $this->assertEquals($hand, $player->getHand());
    }

    public function highCardProvider()
    {
        return [
            '2 of clubs' => [
                'high'  => new Card('club', '2'),
                'cards' => [
                    ['club', '5'],
                    ['club', '7'],
                    ['club', '2'],
                    ['club', '9'],
                    ['club', '10'],
                ]
            ],
            '3 of hearts' => [
                'high'  => new Card('heart', '3'),
                'cards' => [
                    ['diamond', '2'],
                    ['diamond', 'ace'],
                    ['heart', '3'],
                    ['diamond', 'king'],
                    ['diamond', 'queen'],
                ]
            ],
        ];
    }

    /**
     * @dataProvider highCardProvider
     */
    public function testShouldFindHighCard(Card $card, array $cards)
    {
        $cards = array_map(function ($item) {
            return new Card($item[0], $item[1]);
        }, $cards);

        $player = new Player($cards);

        $this->assertEquals($card, $player->getHighCard());
    }

    public function testShouldCalculateScoreFromBestHand()
    {
        $player = new Player([
            new Card('heart', 'ace'),
            new Card('diamond', 'ace'),
            new Card('club', 'ace'),
            new Card('heart', '10'),
            new Card('club', '9'),
        ]);

        $this->assertEquals(4000 + 400 + 12, $player->getScore());
    }
}
