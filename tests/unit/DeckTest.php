<?php

namespace Poker\Test\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Poker\Deck;
use Poker\Card;

class DeckTest extends TestCase
{
    public function testHasAllSuitsAndRanks()
    {
        $deck = Deck::create();

        $this->assertEquals(52, $deck->getCardCount());

        $actual   = $deck->draw($deck->getCardCount());
        $expected = [];

        foreach (Card::SUITS as $suit => $_) {
            foreach (Card::RANKS as $rank => $_) {
                $expected[] = new Card($suit, $rank);
            }
        }

        $this->assertEquals($expected, $actual);
    }

    public function testCanShuffleCards()
    {
        $deck = Deck::create();
        $card = new Card('spade', '2');

        $deck->shuffle();

        $this->assertNotEquals($card, $deck->draw(1));
    }

    public function drawDataProvider()
    {
        return [
            '0 cards' => [
                'count' => 0,
                'throws exception' => true,
            ],
            '53 cards' => [
                'count' => 53,
                'throws exception' => true,
            ],
            '1 card' => [
                'count' => 1,
                'throws exception' => false
            ]
        ];
    }

    /**
     * @dataProvider drawDataProvider
     */
    public function testCannotDrawMoreThan52Cards(int $count, bool $throwsException)
    {
        if ($throwsException) {
            $this->expectException(\LogicException::class);
        }

        $cards = Deck::create()->draw($count);

        if (!$throwsException) {
            $this->assertCount($count, $cards);
        }
    }
}
